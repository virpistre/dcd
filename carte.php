<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css"
        integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
    <link rel="stylesheet" href="style.css">
    <title>Presentation</title>
</head>

<body>
    <header>
        <nav role="navigation">
			<div id="menuToggle">
				<!-- A fake / hidden checkbox is used as click reciever, so you can use the :checked selector on it -->
				<input type="checkbox" />
				
				<!-- Some spans to act as a hamburger
				They are acting like a real hamburger, not that McDonalds stuff -->
				<span> </span>
				<span> </span>
				<span> </span>
				
				<!-- Too bad the menu has to be inside of the button but hey, it's pure CSS magic -->
				<ul id="menu">
					<a href="dcd.php"><li>ACCUEIL</li></a>
					<a href="presentation.html"><li>PRESENTATION</li></a>
					<a href="carte.php"><li>CARTE</li></a>
				</ul>
			</div>
		</nav>
    </header>
    <main>
        <div class="container" id="burger_dispo">
            <div class="row" >
              
                  <?php
                  $menu = array(
                            "BACON LOVER" =>array(
                              "dispo"=> true,
                              "image"=> "http://www.hamburgerfinder.fr/wp-content/uploads/Burger-King-Bacon-Lover-300x231.jpg",
                              "prix"=> "10€"),
                            "CHEESE & BACON"=>array(
                              "dispo"=> true,
                              "image"=> "http://www.hamburgerfinder.fr/wp-content/uploads/Burger-King-BBQ-Cheese-Bacon-300x231.jpg",
                              "prix"=>  "11€"),
                            "BEEF BBQ"=>array(
                              "dispo"=> true,
                              "image"=>"http://www.hamburgerfinder.fr/wp-content/uploads/McDonalds-Beef-BBQ-300x231.jpg",
                              "prix"=> "12€"),
                            "CHICKEN"=>array(
                              "dispo"=> true,
                              "image"=>"http://www.hamburgerfinder.fr/wp-content/uploads/McDonalds-Chicken-Big-Tasty-300x231.jpg",
                              "prix"=> "11€"),
                            "FISH"=>array(
                              "dispo"=> false,
                              "image"=>"http://www.hamburgerfinder.fr/wp-content/uploads/Burger-King-Chicken-TenderCrisp-300x231.jpg",
                              "prix"=>"10€"),
                            "CHEDDAR & BACON"=>array(
                              "dispo"=> false,
                              "image"=>"http://www.hamburgerfinder.fr/wp-content/uploads/McDonalds-Cheddar-Smoky-Bacon-300x231.jpg",
                              "prix"=>"11€"), 
                            "BIG MAC"=>array(
                              "dispo"=> false,
                              "image"=>"http://www.hamburgerfinder.fr/wp-content/uploads/McDonalds-Big-Mac-300x231.jpg",
                              "prix"=> "12€"),
                            "BIG BURGER"=>array(
                              "dispo"=> false,
                              "image"=> "http://www.hamburgerfinder.fr/wp-content/uploads/McDonalds-Big-Mac-300x231.jpg",
                              "prix"=> "11€")
                  );
    
                  foreach($menu as $key=>$value){
                   ?>
              <div class="col-xl-2">      
                <div class="nom_burger">
                  <?php echo $key;
                  ?>
                </div>
                <?php 
                if ($value['dispo']){
                    echo "DISPONIBLE";}
                    else{
                        echo "INDISPONIBLE";
                    }
                ?>
                <div class="image_menu">
                  <img class="img-fluid" src=<?php echo $value['image']?> alt="photo burger">
                </div>
                
                <div>
                        Cum haec taliaque sollicitas eius aures everberarent expositas semper eius modi rumoribus et patentes, varia animo tum miscente consilia, tandem id ut optimum factu elegit.
                </div>    
                <div class="prix_burger">
                  <?php echo $value['prix']?>
                </div>
                  
                  
              </div>
            
                  <?php
                }
                ?>
            </div>
          </div>
    
        <!--<div class="container">
            <div class="row">
                <div class="col-xl-2">
                    <div class="nom_burger">
                        BACON LOVER
                    </div>
                    <div class="image_menu">
                       
                        <img class="img-fluid" src="http://www.hamburgerfinder.fr/wp-content/uploads/Burger-King-Bacon-Lover-300x231.jpg" alt="photo burger">
                        <div>
                        Cum haec taliaque sollicitas eius aures everberarent expositas semper eius modi rumoribus et patentes, varia animo tum miscente consilia, tandem id ut optimum factu elegit.
                        </div>    
                    </div>
                    <div class="prix_burger">
                        10€
                    </div>
                </div>
                <div class="col-xl-2">
                    <div class="nom_burger">
                        CHEESE & BACON
                    </div>
                    <div class="image_menu">
                        <img class="img-fluid"
                            src="http://www.hamburgerfinder.fr/wp-content/uploads/Burger-King-BBQ-Cheese-Bacon-300x231.jpg" alt="photo burger">
                            <div>
                                Cum haec taliaque sollicitas eius aures everberarent expositas semper eius modi rumoribus et patentes, varia animo tum miscente consilia, tandem id ut optimum factu elegit.
                                </div>    
                        </div>
                    <div class="prix_burger">
                        11€
                    </div>

                </div>
                <div class="col-xl-2">
                    <div class="nom_burger">
                        BEEF BBQ
                    </div>
                    <div class="image_menu">
                        <img class="img-fluid" src="http://www.hamburgerfinder.fr/wp-content/uploads/McDonalds-Beef-BBQ-300x231.jpg" alt="photo burger">
                        <div>
                            Cum haec taliaque sollicitas eius aures everberarent expositas semper eius modi rumoribus et patentes, varia animo tum miscente consilia, tandem id ut optimum factu elegit.
                            </div>    
                    </div>
                    <div class="prix_burger">
                        12€
                    </div>

                </div>
                <div class="col-xl-2">
                    <div class="nom_burger">
                        CHICKEN
                    </div>
                    <div class="image_menu">
                        <img class="img-fluid"
                            src="http://www.hamburgerfinder.fr/wp-content/uploads/McDonalds-Chicken-Big-Tasty-300x231.jpg" alt="photo burger">
                            <div>
                                Cum haec taliaque sollicitas eius aures everberarent expositas semper eius modi rumoribus et patentes, varia animo tum miscente consilia, tandem id ut optimum factu elegit.
                                </div>    
                        </div>
                    <div class="prix_burger">
                        11€
                    </div>

                </div>
           

        <div class="col-xl-2">
            <div class="nom_burger">
                FISH
            </div>
            <div class="image_menu">
                <img class="img-fluid" src="http://www.hamburgerfinder.fr/wp-content/uploads/Burger-King-Chicken-TenderCrisp-300x231.jpg" alt="photo burger">
            
                <div>
                    Cum haec taliaque sollicitas eius aures everberarent expositas semper eius modi rumoribus et patentes, varia animo tum miscente consilia, tandem id ut optimum factu elegit.
                    </div> 
                    </div>   
            <div class="prix_burger">
                10€
            </div>

        </div>
        <div class="col-xl-2">
            <div class="nom_burger">
                CHEDDAR & BACON
            </div>
            <div class="image_menu">
                <img class="img-fluid" src="http://www.hamburgerfinder.fr/wp-content/uploads/McDonalds-Cheddar-Smoky-Bacon-300x231.jpg" alt="photo burger" >
                <div>
                    Cum haec taliaque sollicitas eius aures everberarent expositas semper eius modi rumoribus et patentes, varia animo tum miscente consilia, tandem id ut optimum factu elegit.
                    </div>    
            </div>
            <div class="prix_burger">
                11€
            </div>

        </div>
        <div class="col-xl-2">
            <div class="nom_burger">
                BIG MAC
            </div>
            <div class="image_menu">
                <img class="img-fluid" src="http://www.hamburgerfinder.fr/wp-content/uploads/McDonalds-Big-Mac-300x231.jpg" alt="photo burger">
                <div>
                    Cum haec taliaque sollicitas eius aures everberarent expositas semper eius modi rumoribus et patentes, varia animo tum miscente consilia, tandem id ut optimum factu elegit.
                    </div>    
            </div>
            <div class="prix_burger">
                12€
            </div>

        </div>
        <div class="col-xl-2">
            <div class="nom_burger">
                BIG BURGER
            </div>
            <div class="image_menu">
                <img class="img-fluid" src="http://www.hamburgerfinder.fr/wp-content/uploads/McDonalds-Big-Mac-300x231.jpg" alt="photo burger">
                <div>
                    Cum haec taliaque sollicitas eius aures everberarent expositas semper eius modi rumoribus et patentes, varia animo tum miscente consilia, tandem id ut optimum factu elegit.
                    </div>    
            </div>
            <div class="prix_burger">
                11€
            </div>

        </div>
       </div>
    </div>-->
    </main>
</body>

</html>