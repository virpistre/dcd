

          function ouvert(debut,fin,act){
        if (debut > act){
          return "Actuellement Fermé";
        } 
        else if (fin < act){
          return "Actuellement Fermé";}
        else{
          return "Actuellement Ouvert";}
          
      }
    //jours de la semaine
        var day = [
          ["11:00:00", "14:00:00", "18:00:00", "23:30:00"],//dimanche
          ["11:30:00", "14:00:00", "18:00:00", "22:00:00"],//lundi
          ["11:30:00", "14:00:00", "18:00:00", "22:00:00"],//mardi
          ["11:30:00", "13:00:00", "18:00:00", "22:00:00"],//mercredi
          ["11:30:00", "14:00:00", "18:00:00", "22:00:00"],//jeudi
          ["11:30:00", "14:00:00", "18:00:00", "23:30:00"],//vendredi
          ["11:00:00", "14:00:00", "18:00:00", "23:30:00"]//samedi
        ]
          
        var dayDay = new Date();
    
    //Récup jour de la semaine (chiffre entre 0 et 6 => correspondant au tableau du dessus)
        var dayNow = dayDay.getDay();
      
      //heure actuelle transformé en minutes
        //Récup heure actuelle (heures + minutes)
        var hoursNow = dayDay.getHours();
        var minutesNow = dayDay.getMinutes();
      
    //calcul le nombre de minutes complète 
    function minutes_totales(a, b){
        var x = a*60 + b;
        return x;
    }
      
    var minutes_actus = minutes_totales(hoursNow, minutesNow);
          
      //récupérer et changer les string en int des heures de services du tableau day
      
      function change_string(a){
        var separation = a.split(':');
        
        var h = parseInt(separation[0]);
        var m = parseInt(separation[1]);
        
        var minutes_service = minutes_totales(h, m);
        return minutes_service;
      }
       
      ////////////////////////////////////////////////////////
            
      var h2 = document.getElementById('ouverture-fermeture-actuelle');
    
    
      //utilisation de la fonction de changement des string en int et de la fonction qui calcul si le resto est actuellement fermé ou ouvert
      
      if(0 < dayNow < 6){
        var jour = day[dayNow][1]; //fin de service du matin en string
        
        var fin_serv_mat = change_string(jour);
        
        if(minutes_actus < fin_serv_mat){
          
          var hdm = change_string(day[dayNow][0]);
          var hfm = change_string(day[dayNow][1]);
          
          var v = ouvert(hdm, hfm, minutes_actus);
          h2.innerText = v;   
        }
        else if (minutes_actus > fin_serv_mat){
          
          var hda = change_string(day[dayNow][2]);
          var hfa = change_string(day[dayNow][3]);
          
          var v = ouvert(hda, hfa, minutes_actus);
          h2.innerText = v;
        }
      }
    
      ///////////////////////////////////////////////////////
    
      var tableau = document.getElementById('tableau');
    
      var jouJour = [
          "Dimanche", "Lundi", "Mardi", "Mercredi", "Jeudi", "Vendredi", "Samedi"
      ]
    
      for (i=0; i<7; i++){
          
        y=0
    
        var op = day[i][y].substr(0, 5);
        var fer = day[i][y+1].substr(0, 5);
    
        var newLigne = document.createElement('tr');
        tableau.appendChild(newLigne);
    
        var colonne1 = document.createElement('td');
        colonne1.innerText = jouJour[i];
        newLigne.appendChild(colonne1);
    
        var colonne2 = document.createElement('td');
            colonne2.innerText = op + ' - ' + fer;
        newLigne.appendChild(colonne2);
    
        var en = day[i][y+2].substr(0, 5);
        var mer = day[i][y+3].substr(0, 5);
    
        var colonne3 = document.createElement('td');
             colonne3.innerText = en + ' - ' + mer;
        newLigne.appendChild(colonne3);
      }

