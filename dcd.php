<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
  
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.min.js" integrity="sha384-+YQ4JLhjyBLPDQt//I+STsc9iw4uQqACwlvpslubQzn4u2UU2UFM80nGisd026JF" crossorigin="anonymous"></script>
<link rel="stylesheet" href="style.css">
<title>dcd</title>

</head>
<body>
    <header>  
      <nav role="navigation">
			  <div id="menuToggle">
				  <!-- A fake / hidden checkbox is used as click reciever, so you can use the :checked selector on it -->
				  <input type="checkbox" />
				
				  <!-- Some spans to act as a hamburger
				  They are acting like a real hamburger, not that McDonalds stuff -->
				  <span> </span>
				  <span> </span>
				  <span> </span>
				
				  <!-- Too bad the menu has to be inside of the button but hey, it's pure CSS magic -->
          <ul id="menu">
            <a href="dcd.php"><li>ACCUEIL</li></a>
            <a href="presentation.html"><li>PRESENTATION</li></a>
            <a href="carte.php"><li>CARTE</li></a>
          </ul>
			  </div>
		  </nav>
    </header>
    <main>
      <div class="container" id="parent">
        <div class="row">
          <div class="col-xl-6" id="gauche">
            <div id="horaire">
              <h2 id="ouverture-fermeture-actuelle"></h2>
            </div>
            <div>
                <!--<div class="container">
                <div class="row justify-content-md-center gy-5">
                  <div class="col-8 p-4">
                    <h2 id="ouverture-fermeture-actuelle"></h2>
                </div>
                </div>
                </div>

                <div class="container">
                    <div class="row justify-content-md-center gy-5">
                        <div class="col-8 p-4">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>Jour</th>
                                    <th></th>
                                    <th></th>
                                </tr>
                                </thead>

                                <tbody id="tableau">
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>-->

                <div class="container">
                  <div class="row justify-content-md-center gy-5">
                    <div>
                      <table class="table">
                        <tbody id="tableau">
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
           
                  <!-- <ul>  </ul>
                    <ul>lundi 11h/14h 18h/23h</ul> 
                    <ul>mardi 11h/14h 18h/23h</ul>
                    <ul>mercredi 11h/14h 18h/23h</ul>
                    <ul>jeudi 11h/14h 18h/23h</ul>
                    <ul>vendredi 11h/14h 18h/23h</ul>
                    <ul>samedi 11h/14h 18h/23h</ul>
                    <ul>dimanche 11h/14h 18h/23h</ul>-->
                    
                <script type="text/javascript" >

                  window.open("commander.html", "nomdelafenetrepopup", "height=200, width=300, location='yes|no', status='yes|no', scrollbars='yes|no'");

                </script>
             
            </div>
            <div id="message">

            </div>
          </div>
          <div class="col-xl-6" id="droite">
            <div id="map">
              <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2888.8117459342434!2d2.2240997505076376!3d43.61046116310986!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x12ae11fec97cbc07%3A0x684fd16208f2b8!2sAv.%20de%20Lavaur%2C%2081100%20Castres!5e0!3m2!1sfr!2sfr!4v1627292292291!5m2!1sfr!2sfr" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
            </div>
            <div id="telephone">
              <img id="tel" src="tel.jpg">
              <a class="listButton" href="tel:+330123456789"> TELEPHONE : 0123456789</a>
            </div>
          </div>
        </div>
      </div>
      <div class="container" id="burger_dispo">
        <div class="row" >
          
              <?php
              $menu = array(
                        "BACON LOVER" =>array(
                          "dispo"=> true,
                          "image"=> "http://www.hamburgerfinder.fr/wp-content/uploads/Burger-King-Bacon-Lover-300x231.jpg",
                          "prix"=> "10€"),
                        "CHEESE & BACON"=>array(
                          "dispo"=> true,
                          "image"=> "http://www.hamburgerfinder.fr/wp-content/uploads/Burger-King-BBQ-Cheese-Bacon-300x231.jpg",
                          "prix"=>  "11€"),
                        "BEEF BBQ"=>array(
                          "dispo"=> true,
                          "image"=>"http://www.hamburgerfinder.fr/wp-content/uploads/McDonalds-Beef-BBQ-300x231.jpg",
                          "prix"=> "12€"),
                        "CHICKEN"=>array(
                          "dispo"=> true,
                          "image"=>"http://www.hamburgerfinder.fr/wp-content/uploads/McDonalds-Chicken-Big-Tasty-300x231.jpg",
                          "prix"=> "11€"),
                        "FISH"=>array(
                          "dispo"=> false,
                          "image"=>"http://www.hamburgerfinder.fr/wp-content/uploads/Burger-King-Chicken-TenderCrisp-300x231.jpg",
                          "prix"=>"10€"),
                        "CHEDDAR & BACON"=>array(
                          "dispo"=> false,
                          "image"=>"http://www.hamburgerfinder.fr/wp-content/uploads/McDonalds-Cheddar-Smoky-Bacon-300x231.jpg",
                          "prix"=>"11€"), 
                        "BIG MAC"=>array(
                          "dispo"=> false,
                          "image"=>"http://www.hamburgerfinder.fr/wp-content/uploads/McDonalds-Big-Mac-300x231.jpg",
                          "prix"=> "12€"),
                        "BIG BURGER"=>array(
                          "dispo"=> false,
                          "image"=> "http://www.hamburgerfinder.fr/wp-content/uploads/McDonalds-Big-Mac-300x231.jpg",
                          "prix"=> "11€")
              );

              foreach($menu as $key=>$value){
                if ($value['dispo']){?>
          <div class="col-xl-2">      
            <div class="nom_burger">
              <?php echo $key;
              ?>
            </div>
            <div class="image_menu">
              <img class="img-fluid" src=<?php echo $value['image']?> alt="photo burger">
            </div>
            <div class="prix_burger">
              <?php echo $value['prix']?>
            </div>
              <button class="button_ubereats" onclick="window.location.href = '//www.ubereats.com/fr';">COMMANDER</button>
              <?php
                } ?>
          </div>
        
              <?php
            }
            ?>
        </div>
      </div>

          <!--<div class="nom_burger">
          BACON LOVER
          </div>
          <div class="image_menu">
            
            <img class="img-fluid" src="http://www.hamburgerfinder.fr/wp-content/uploads/Burger-King-Bacon-Lover-300x231.jpg" alt="photo burger">
          </div>
          <div class="prix_burger">
            10€
          </div>
          <button class="button_ubereats" onclick="window.location.href = '//www.ubereats.com/fr';">COMMANDER</button>
        </div>
        <div class="col-xl-2">
          <div class="nom_burger">
            CHEESE & BACON
          </div>
          <div class="image_menu">
            <img class="img-fluid" src="http://www.hamburgerfinder.fr/wp-content/uploads/Burger-King-BBQ-Cheese-Bacon-300x231.jpg" alt="photo burger">
          </div>
          <div class="prix_burger">
            11€
          </div>
          <button class="button_ubereats" onclick="window.location.href = '//www.ubereats.com/fr';">COMMANDER</button>
        </div>
        <div class="col-xl-2">
          <div class="nom_burger">
             BEEF BBQ
          </div>
          <div class="image_menu">
            <img class="img-fluid" src="http://www.hamburgerfinder.fr/wp-content/uploads/McDonalds-Beef-BBQ-300x231.jpg" alt="photo burger">
          </div>
          <div class="prix_burger">
            12€
          </div>
          <button class="button_ubereats" onclick="window.location.href = '//www.ubereats.com/fr';">COMMANDER</button>
        </div>
        <div class="col-xl-2">
          <div class="nom_burger">
            CHICKEN
          </div>
          <div class="image_menu">
            <img class="img-fluid" src="http://www.hamburgerfinder.fr/wp-content/uploads/McDonalds-Chicken-Big-Tasty-300x231.jpg" alt="photo burger">
          </div>
          <div class="prix_burger">
            11€
          </div>
          <button class="button_ubereats" onclick="window.location.href = '//www.ubereats.com/fr';">COMMANDER</button>-->
      
    </main>
        <script type="text/javascript" src="ouvert.js"></script>
  </body>
</html>